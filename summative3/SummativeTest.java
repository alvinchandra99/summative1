import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SummativeTest {

    @InjectMocks
    SummativeIntermediate summative = new SummativeIntermediate();

    @Test
    public void Test(){
   
       String[] args = {"10", "20"};
       Assert.assertEquals(summative.main(args), "30");

       

    }
    
}
