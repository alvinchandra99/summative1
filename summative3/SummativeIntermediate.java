

public class SummativeIntermediate{
    public static int main(String[] args) {
        int total = 0;
        for(int i = 0; i < args.length; i ++){
            total = Integer.parseInt(args[i]) + total;
        }
        return total;
    }
}