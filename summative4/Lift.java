import java.util.HashMap;
import java.util.List;
public class Lift {
    private int posisiLift;
    private List<Integer> posisi;
    private List<Integer> tujuan;
    private int tujuanTerdekat;

    Lift(int posisiLift,List<Integer> posisiCustomer, List<Integer> tujuanCustomer){
        this.posisiLift = posisiLift;
        this.posisi = posisiCustomer;
        this.tujuan = tujuanCustomer;
    }

    void customerTerdekat(){
        int distance = Math.abs(posisi.get(0) - posisiLift);
        int idx =0;


        for(int c = 0 ; c< posisi.size(); c++){
            int cdistance = Math.abs(posisi.get(c) - posisiLift );
            if(cdistance < distance){
                idx = c;
                distance = cdistance;
            }
        }
        int tujuanTerdekat = posisi.get(idx);
        this.posisi.remove(tujuanTerdekat);
        this.tujuanTerdekat = tujuanTerdekat;
    }
    

    void getCustomer(){
        customerTerdekat();

        System.out.print("Start -> ");
        if(tujuanTerdekat - posisiLift < 0){
            down();
            System.out.print(" -> Get Customer");
        }
        if(tujuanTerdekat - posisiLift > 0){
            up();
        }

        
    }

    void down(){
        for(int i = posisiLift ; i > tujuanTerdekat ; i--){
            this.posisiLift = i;
            System.out.println(namaLantai(i));
        }
    }

    void customerTerdekat2(){
        int distance = Math.abs(posisi.get(0) - posisiLift);
        int idx =0;


        for(int c = 0 ; c< posisi.size(); c++){
            int cdistance = Math.abs(posisi.get(c) - posisiLift );
            if(cdistance < distance){
                idx = c;
                distance = cdistance;
            }
        }
        int tujuanTerdekat = posisi.get(idx);

        int distance2 = Math.abs(tujuan.get(0) - posisiLift);
        int idx2 =0;


        for(int c = 0 ; c< posisi.size(); c++){
            int cdistance2 = Math.abs(tujuan.get(c) - posisiLift );
            if(cdistance2 < distance2){
                idx2 = c;
                distance2 = cdistance2;
            }
        }
        int tujuanTerdekat2 = tujuan.get(idx2);

        this.tujuanTerdekat = Math.min (tujuanTerdekat, tujuanTerdekat2);


        
    }

    void up(){
        for(int i = posisiLift ; i < tujuanTerdekat ; i++){
            this.posisiLift = i;
            System.out.println(namaLantai(i));
        }

    }

    String namaLantai(int posisi){
        String nama = "";
        switch(posisi){
            case 1:
                nama = "Lantai Parkir";
                break;
            case 2:
                nama = "GF";
                break;
            case 3 :
                nama = "Lantai 1";
                break;
            case 4:
                nama = "Lantai 2";
                break;
            case 5:
                nama = "Lantai 3";
                break;
        }
        return nama;
    }
}
