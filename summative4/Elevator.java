import java.util.List;

public class Elevator {
    private int posisiLift;
    private List<Integer> posisi;
    private List<Integer> tujuan;
    private int tujuanTerdekat;
    
    Elevator(int posisiLift,List<Integer> posisi, List<Integer> tujuan){
        this.posisiLift = posisiLift;
        this.posisi = posisi;
        this.tujuan = tujuan;
    }

    void algorithm(){
        checkCustomerTerdekat();
        
        if(tujuanTerdekat > posisiLift){
            ElevatorUp();
        }

        if(tujuanTerdekat < posisiLift){
            ElevatorDown();
        }

        if(posisi.size() == 0 & tujuan.size() ==0){
            return;
        }


        algorithm();
    }

    void checkCustomerTerdekat(){
        int distance = Math.abs(posisi.get(0) - posisiLift);
        int idx =0;


        for(int c = 0 ; c< posisi.size(); c++){
            int cdistance = Math.abs(posisi.get(c) - posisiLift );
            if(cdistance < distance){
                idx = c;
                distance = cdistance;
            }
        }
        int tujuanTerdekat = posisi.get(idx);
        this.posisi.remove(tujuanTerdekat);
        this.tujuanTerdekat = tujuanTerdekat;
        

    }


    void getCustomer(){
        System.out.println("get customer");

    }

    void dropCustomer(){
        System.out.println("drop customer");

    }

    void ElevatorUp(){
        for(int i = posisiLift ; i < tujuanTerdekat ; i++){
            for(int j : tujuan){
                if(i == j){
                    dropCustomer();
                    this.tujuan.remove(j);
                }
            }
            this.posisiLift = i;
            System.out.println(namaLantai(i));
        }

    }

    void ElevatorDown(){
        for(int i = posisiLift ; i > tujuanTerdekat ; i++){
            for(int j : tujuan){
                if(i == j){
                    dropCustomer();
                    this.tujuan.remove(j);
                }
            }
            this.posisiLift = i;
            System.out.println(namaLantai(i));
        }


    }



    String namaLantai(int posisi){
        String nama = "";
        switch(posisi){
            case 1:
                nama = "Lantai Parkir";
                break;
            case 2:
                nama = "GF";
                break;
            case 3 :
                nama = "Lantai 1";
                break;
            case 4:
                nama = "Lantai 2";
                break;
            case 5:
                nama = "Lantai 3";
                break;
        }
        return nama;
    }

    
}
