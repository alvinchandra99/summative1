import java.util.Scanner;

public class Main{
    public static double BiayaParkir(int kendaraan, int waktuParkir){
        int waktuPakirMinutes =0;
        int firstHour = 0;
        int nextHour = 0;
        double sisaWaktu;
        double biaya = 0;

        // Menentukan Tarif
        if(kendaraan ==1){
            firstHour = 5000;
            nextHour = 4500;
        }
        if(kendaraan == 2){
            firstHour = 4000;
            nextHour = 3500;
        }

        switch(waktuParkir){
            case 1:
                waktuPakirMinutes = 30;
                break;
            case 2:
                waktuPakirMinutes = 62;
                break;
            case 3:
                waktuPakirMinutes = 240;
                break;
            case 4:
                waktuPakirMinutes = 419;
                break;
            case 5:
                waktuPakirMinutes = 2400;
                break;
        }

        if(waktuPakirMinutes > 0 && waktuPakirMinutes <= 60){
            biaya = firstHour;
            return biaya;
        }
        if(waktuPakirMinutes > 60){
            sisaWaktu =(double) ((waktuPakirMinutes -60)/ 60);
            biaya = firstHour + (Math.ceil(sisaWaktu) * nextHour);
            return biaya;
        }

        return biaya;


    }
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        // Kendaraan yang digunakan
        System.out.println("Kendaraan yang digunakan : \n" +
                        "1. Mobil : \n"+
                        "2. Motor : ");
        System.out.println("Pilih Kendaraan yang digunakan : ");
        int kendaraan = userInput.nextInt();

        // Waktu Parkir
        System.out.println("-------------------------------");
        System.out.println("Tentukan Lama parkir : \n" +
                            "1. 30 Menit \n"+
                            "2. 1 Jam 2 Menit \n" +
                            "3. 4 Jam \n" +
                            "4. 6 Jam 59 Menit \n"+
                            "5. 40 Jam ");
        System.out.print("Tentukan waktu Parkir : ");
        int waktuParkir = userInput.nextInt();
        System.out.println("-------------------------------");

        System.out.println("Biaya Parkir : ");
        System.out.print(BiayaParkir(kendaraan, waktuParkir));
    }
}